triplets :: Int -> [[Int]]
triplets x = [[a, b, c] | a <- [3..x], b <- [a+1..x], let c = x - a - b, c > 5, a * a + b * b == c * c]

main = do
    putStrLn $ show $ product $ head $ triplets 1000 