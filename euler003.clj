(defn factors [n]
  ;finding factors for a given number
  
  (filter #(zero? (rem n %)) (range 1 (inc n)))
)

(defn isPrime? [n]
  ;checking whether the numbers are prime or not
  
  (= (factors n) [1, n])
)

(defn primeFactors [n]
  ;filtering all the prime factors
  
  (filter #((isPrime? x) (def x factors n)))
)
