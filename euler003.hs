factors n = [x | x <- [1..n], (mod n x) == 0]
isPrime n = factors n == [1, n]
  
primeFactors n = [x | x <- factors n , isPrime x]
   
main = do
       print(max(primeFactors 600851475143))
