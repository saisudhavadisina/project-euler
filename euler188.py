import math, sys


def gen_tetration():
	x, y, m = 1777, 1855, 10**8
	sys.setrecursionlimit(y + 30)
	ans = tetration_mod(x, y, m)
	return str(ans)


def tetration_mod(x, y, m):
	if y == 1:
		return x % m
	else:
		return pow(x, tetration_mod(x, y - 1, totient(m)), m)

def totient(n):
	assert n > 0
	p = 1
	i = 2
	end = math.sqrt(n)
	while i <= end:
		if n % i == 0: 
			p *= i - 1
			n //= i
			while n % i == 0:
				p *= i
				n //= i
			end = math.sqrt(n)
		i += 1
	if n != 1:
		p *= n - 1
	return p

print(gen_tetration())
