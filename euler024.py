#What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?

from math import factorial

def permutation_of_digit(n, s):

   if len(s)==1: return s

   q, r = divmod(n, factorial(len(s)-1))

   return s[q] + permutation_of_digit(r, s[:q] + s[q+1:])



LIMIT = 1000000

n currency}

def dynamic(coin):

    total = currency[coin]

    pieces = sorted(reverse)
    counts = [0 for i in range(total+1)]
    counts[0] = 1
    for p in pieces:
        for med in range(p, total+1):
            counts[med] += counts[med-p]

    return counts[-1]

print( dynamic(u'£2'))p]]]]]r]int ("S lution = ", permutation_of_digit(LIMIT-1, '0123456789')s
