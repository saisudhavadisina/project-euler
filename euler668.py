import math
import sympy
def divisible(number):
    LIMIT = (int)(math.sqrt(number))
    return [i for i in range(2, LIMIT + 1) if (number % i == 0)]

def factors(number):
    return [i for i in range(number) if max(divisible(number)) < int(math.sqrt(number))]

def squareRootSmooth(number):
    return [i for i in range(number) if sympy.isprime(i)]

def noOfSquareRootSmooth(number):
    count = 0
    for i in range(1, number):
        if(squareRootSmooth(i)):
            count = count + 1
    return count
print(factors(100))
print(noOfSquareRootSmooth(100))
