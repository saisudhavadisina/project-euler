isPalindrome n = reverse(n) == n

prod = [x | x == n * m, n <- [1..100], m <- [1..100]]

palindromeProduct = [x | x <- prod, isPalindrome x]
 
main = do
    print(palindromeProduct)
