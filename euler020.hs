fact 0 = 1
fact n = n * fact(n - 1)
--finding factorial for a given number

digits n = n `mod` 10 ++ [digits(n `div` 10)]
--finding the digits in a given number and storing in a list

sumOfDigits n = sum(digits n)
--sum of the digits in a given number
main = do
    print(sumOfDigits 113)
