import functools(a, lcm(b, lcm(c, d)))

def find_lcm(*args):
    return functools.reduce(lcm, args)

def lcm(a, b):
    return a * b // gcd(a, b)

def gcd(a, b):
    return b if a == 0  else return gcd(b % a, a)


solution = find_lcm(*range(1, 20))
print('solution: ', solution)

