# There's a recursive formula about the numerator
# n(k) = c(k) * n(k-1) + n(k-2)

def Gen(num):
    yield 2
    index = 1
    while True:
        index += 1
        if index % 3 == 0:
            k = index // 3
            yield k * 2
        else:
            yield 1
        if index == num:
            break

def num_of_digits(n = 100):
    n1 = 2
    n2 = 3
    g = Gen(n)
    for index, c in enumerate(g):
        if index >= 2:
            temp = c * n2 + n1
            n1 = n2
            n2 = temp
    
    return sum(map(int, str(n2)))

resultant = num_of_digits()
print(resultant)
