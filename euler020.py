def factorial(number):
    if number == 1 or number == 0:
        return number
    return number * factorial(number - 1)

def factorialDigitsList():
    return [int(x) for x in str(factorial(100))]

print(sum(factorialDigitsList()))
