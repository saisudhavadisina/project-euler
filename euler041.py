def is_pandigital(nr, n):
    digits = ''.join(map(str, range(1, n + 1)))
    nr = str(nr)
    for i in digits:
        if str(i) not in nr[0:9]:
            return False
        if str(i) not in nr[-9:]:
            return False
        return True
def is_prime(n):
    if n in [2, 3, 5, 7]:
        return True
    if n % 2 == 0:
        return False
    r = 3
    while r * r <= n:
        if n % r == 0:
            return False
        r += 2
        return True
def pandigitals(LIMIT):
    return [i for i in range(0, LIMIT) if is_pandigital(i, 4) and is_prime(i)]

'''for i in range(100000, 999999):
if is_pandigital(i, 6):
pandigitals.append(i)'''
print(pandigitals(10000))
