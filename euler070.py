import math


def totient_permutations():
	totients = math.list_totients(10**7 - 1)
	min_numer = 1
	min_denom = 0
	for (i, tot) in enumerate(totients[2 : ], 2):
		if i * min_denom < min_numer * tot and sorted(str(i)) == sorted(str(tot)):
			min_numer = i
			min_denom = totients[i]
	return str(min_numer)

print(totient_permutations())

