def collatz(n:int):
    sequence = []
    while True:
        sequence.append(n)
        if n == 1:
            return sequence

        elif n % 2 == 0:
            n = n // 2

        else:
            n = 3 * n + 1
if __name__ == "__main__":
    n = 999999
    current_best = 0

    for i in range(1, 999999):
        sequence = collatz(i)

        if len(sequence) > current_best:
            current_best = len(sequence)
            winner = i

    print(f"The number {winner} produces the longest Collatz sequence!")
    print(f"Length of longest chain: {current_best}")
