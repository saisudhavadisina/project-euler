#9 pythagorean triplet


"""
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which, a ^ 2 + b ^ 2 = c ^ 2
For example, 32 + 42 = 9 + 16 = 25 = 52.
There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc. 
"""

def pythagoreanTriplet(a, b):
    return a * a + b * b == (1000 - a - b) ** 2

x = range(3, 500)
[(x, y, (1000 - x - y), x * y * (1000 - x - y)) for x in x for y in range(x, 500) if pythagoreanTriplet(x, y)]

