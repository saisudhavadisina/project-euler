factors n = [x | x <- [1..n], n `mod` x == 0]
isPrime n = factors n == [1, n]
primes = [x | x <- [1..], isPrime(x)]
main = do
    print(primes !! 10000)
