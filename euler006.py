#6 Sum Of Squares Difference

import math

def sumOfSquares(LIMIT):
    return (LIMIT * (LIMIT + 1) * (2 * LIMIT + 1)) / 6

def sumOfConsecutives(LIMIT):
    return (LIMIT * (LIMIT + 1)) / 2

def sumSquareDifference():
    LIMIT = 100
    return math.pow(sumOfConsecutives(LIMIT), 2) - sumOfSquares(LIMIT)

int(sumSquareDifference())
