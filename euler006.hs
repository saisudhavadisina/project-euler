n *** 2 = n * n
--finding square of n

squaresSum n = sum([x *** 2 | x <- [1..n]])
--finding sum of the squares of the first n natural numbers

nSumSquares x = sum([1..x]) *** 2
-- finding square of the sum of the first n natural numbers

diff x = nSumSquares x - squaresSum x

main = do
    print(diff 100)
