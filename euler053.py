"""There are exactly ten ways of selecting three from five, 12345:
123, 124, 125, 134, 135, 145, 234, 235, 245, and 345
In combinatorics, we use the notation, (53)=10.
In general, (nr)=n!r!(n−r)!, where r≤n, n!=n×(n−1)×...×3×2×1, and 0!=1.
It is not until n=23, that a value exceeds one-million: (2310)=1144066.
How many, not necessarily distinct, values of (nr) for 1≤n≤100, are greater than one-million?"""

currency = {u'£2': 200, u'£1': 100, '50p': 50, '20p': 20,
             '10p': 10, '5p': 5, '2p': 2, '1p': 1}
reverse = {currency[k]:k for k in currency}

def dynamic(coin):
    total = currency[coin]
    pieces = sorted(reverse)
    counts = [0 for i in range(total+1)]
    counts[0] = 1
    for p in pieces:
        for med in range(p, total+1):
            counts[med] += counts[med-p]

    return counts[-1]

print( dynamic(u'£2'))
