#2
def fibonacciSequence(LIMIT):
    sequence1 = []
    sequence1.append(1)
    sequence1.append(2)
    [sequence1.append(sequence1[i - 1] + sequence1[i - 2]) for i in range(2, LIMIT)]
    return sum((sequence1[i] for i in range(1, LIMIT)), 2)

fibonacciSequence(100)     
