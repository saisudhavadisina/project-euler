import math
def powerOfNum(number):
    power = math.pow(2, number)
    return int(power)

def powerDigitList():
    return [int(x) for x in str(powerOfNum(1000))]

print(sum(powerDigitList()))
