#1
def multiples_3_5(LIMIT):
    return sum(set(range(3,LIMIT,3)) | set(range(5, LIMIT, 5)))

print(multiples_3_5(10000))
