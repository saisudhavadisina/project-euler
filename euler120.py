#Problem 120.

def solve(max_a):
    
    Sum = 0
    for a in range(3,max_a+1):
        Sum += 2*a*((a-1)//2)
    
    return Sum

if __name__ == '__main__':
    Sum = solve(1000) # ~ 128us
    print(Sum)
