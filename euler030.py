#30 
import math
def digit_power_sum(num):
    add = 0
    while(num != 0):
        add = add + math.pow((num % 10), 5)
        num = num // 10
    return add
def digitFifth():
    return [i for i in range(2, 10000000) if(i == digit_power_sum(i))]
print(digitFifth())
print(sum(digitFifth()))

