import math
def isqrt(n: int) -> int:
    return int(math.sqrt(n))
def l_divisors(n : int) -> [int]:
    return[i for i in range(2, isqrt(n) + 1) if n % i == 0]
def u_divisors(n : int) -> [int]:
    return[n // f for f in l_divisors(n)]
def divisors(n : int) -> {int}:
    return set([1]) | set(l_divisors(n)) | set(u_divisors(n))
def amicable(START: int, LIMIT: int) -> [(int, int)]:
    pairs = [(n, sum(divisors(n))) for n in range(START, LIMIT)]
    return [a for a, b in pairs if sum(divisors(b)) == a]

print(amicable(1, 10000))
print(sum(amicable(1, 10000)))
