import math 

def distinctPowers():
    return {int(math.pow(a, b)) for a in range(2, 101) for b in range(2, 101)}

print(len(distinctPowers()))
