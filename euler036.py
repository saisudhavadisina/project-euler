#36
def reverse(num):
    rev = 0
    while(num != 0):
        rev = rev * 10 + (num % 10)
        num = num // 10
    return rev
    
def palindrome(num):
    return (num == reverse(num))

def double_base_palindrome(num):
    return[i for i in range(10, num) if(palindrome(i) and palindrome(binary(i)))]

def binary(num):
    bin = 0
    while(num != 0):
        bin = bin * 10 + num % 2
        num = num // 2
    return bin

num = 10000000
print(double_base_palindrome(num))
print(sum(double_base_palindrome(num)))
