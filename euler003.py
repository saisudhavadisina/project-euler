import  math as np

def is_prime(n):
    if n in [2, 3, 5, 7]:
        return True
    if n % 2 == 0:
        return False
    r = 3
    while r * r <= n:
        if n % r == 0:
            return False
        r += 2
    return True

def find_smallest_prime_factor(number):
    upper_bound = int(np.sqrt(number)) + 1
    for i in range(2, upper_bound):
        if number % i == 0 and is_prime(i):
            return i
    return number

def find_largest_prime_factor(number):
    while True:
        smallest_factor = find_smallest_prime_factor(number)

        if smallest_factor > 1:
            number //= smallest_factor
        else:
            return number

result = find_largest_prime_factor(600851475143)
print('result: ', result)

