(defn palindrome? [n] ;[int] -> [bool]
  ;checking whether reverse is same as that of original or not
  
  (= (reverse n) n)
)

(defn palindrome-number? [n]
  ; to check the number is palindrome or not we are casting the integer in    to string and then to sequence of character 
  
  (palindrome? (seq (str n)))
)

(defn largest_palindrome
  ;to find the largest palindrome generated from two 3-digit numbers 
  
  (reduce max (filter palindrome-number?
            (for [i (range 100 1000) j (range i 1000)] (* i j))
  ))
)
