#45 project euler
def triangular(num):
    return num * (num + 1) / 2

def pentagonal(num):
    return num * (3 * num - 1) / 2

def hexagonal(num):
    return num * (2 * num - 1)

def together(num):
    return [i for i in range(num) if(triangular(i) == pentagonal(i) == hexagonal(i))]

print(together(1000000))
